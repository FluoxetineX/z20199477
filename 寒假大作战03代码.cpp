#include <stdio.h>
#include <stdlib.h>
#include <time.h>
void menu();
void help();
void error();
void First();
void Second();
void Third();
int main()
{
	int n;
	printf("==========口算生成器==========\n");
	printf("欢迎使用口算生成器:");
	printf("\n");
	printf("\n");
	help();
	printf("\n");
	menu();
	scanf_s("%d", &n);
	while (n != 5)
	{
		printf("<执行操作:\n");
		printf("\n");
		switch (n)
		{
		 case 1:
			 First();
			 break;
		 case 2:
			 Second();
			 break;
		 case 3:
			 Third();
			 break;
		 case 4:
			 help();
			 break;
		 default:
			 error();
			 break;
		}
		printf("\n");
		printf("\n");
		menu();
		scanf_s("%d", &n);
	}
	printf("<执行操作:\n");
	printf("\n");
	printf("程序结束，欢迎下次使用，按任意键结束\n");
	return 0;
}
void help()
{
	printf("帮助信息：\n");
	printf("您需要输入命令代号来进行操作，且\n");
	printf("一年级题目为不超过十位的加减法；\n");
	printf("二年级题目为不超过百位的乘除法；\n");
	printf("三年级题目为不超过百位的加减乘除混合题目.\n");
}
void menu()
{
	printf("操作列表：\n");
	printf("1)一年级    2）二年级   3）三年级\n");
	printf("4)帮助      5）退出程序\n");
	printf("请输入操作>\n");
	printf("<执行操作 ：");
}
void error()
{
	printf("Error!!!\n");
	printf("错误操作指令，请重新输入\n");
}
void First()
{
	int a, b, i, m;
	char ch;
	srand((unsigned)time(NULL));
	printf("现在是一年级题目：\n");
	printf("请输入生成个数： ");
	scanf_s("%d", &m);
	printf("<执行操作 ：");
	printf("\n");
	for (i = 0; i < m; i++)
	{
		a = rand() % 10;
		b = rand() % 10;
		if ((a>b) || (a==b)) 
		{
			ch = '-';
		}
		else
		{
			ch = '+';
		}
		printf("%d %c %d = _ \n", a, ch, b);
	}
}
void Second()
{
	int a, b, num, i, m;
	char ch;
	srand((unsigned)time(NULL));
	printf("现在是二年级题目：\n");
	printf("请输入生成个数： ");
	scanf_s("%d", &m);
	printf("<执行操作 ：");
	printf("\n");
	for (i = 0; i < m; i++)
	{
		a = rand() % 100;
		b = rand() % 99 + 1;
		num = rand() % 2;
		if (num == 0)
		{
			ch = '*';
		}
		else
		{
			ch = '/';
		}
		printf("%d %c %d = _ \n", a, ch, b);
	}
}
void Third()
{
	int a, b, c, num, i, m;
	char ch1, ch2;
	srand((unsigned)time(NULL));
	printf("现在是三年级题目：\n");
	printf("请输入生成个数： ");
	scanf_s("%d", &m);
	printf("<执行操作 ：");
	printf("\n");
	for (i = 0; i < m; i++)
	{
		a = rand() % 100;
		b = rand() % 99 + 1;
		c = rand() % 99 + 1;
		num = rand() % 4;
		if (num == 0)
		{
			ch1 = '+';
		}
		else {
			if (num == 1)
			{
				ch1 = '-';
			}
			else {
				if (num == 2)
				{
					ch1 = '*';
				}
				else {
					if (num == 3)
					{
						ch1 = '/';
					}
				}
			}
		}
		num = rand() % 4;
		if (num == 0)
		{
			ch2 = '+';
		}
		else {
			if (num == 1)
			{
				ch2 = '-';
			}
			else {
				if (num == 2)
				{
					ch2 = '*';
				}
				else {
					if (num == 3)
					{
						ch2 = '/';
					}
				}
			}
		}
		printf("%d %c %d %c %d = _ \n", a, ch1, b, ch2, c);
	}
}
