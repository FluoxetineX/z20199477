#include<stdio.h>
#include<string.h>
#include<stdlib.h>
typedef struct person
{
	int number;
	char name[10];
	char phone[12];
}CAI;

CAI book[50];
int b =50, s = 0;//b为可存的容量，s为已存的人数

void Menu();//菜单
void Sort();//排序
void Add();//创建
void Delete();//删除
void Modify();//修改
void Search();//搜索

int main()
{
	Menu();
}

void Menu()
{
	 
	while (1)
	{
		int a, i = 0;
		printf("==========================通讯录==========================\n\n\n\n");
		printf("===========================界面===========================\n\n");
		printf("已存空间： %d人     可存空间：  %d人\n", s, b);

		for (; i < s; i++)
		{
			printf("编号：  %2d  |  姓名：  %8s  |  电话：  %12s\n", book[i].number, book[i].name, book[i].phone);
		}
		printf("\n\n\n\n操作列表：\n\n");
		printf("1)排序    2）添加    3）删除\n");
		printf("4)修改    5) 查找    6）退出程序\n");
		printf("请输入操作： ");
		scanf("%d", &a);
		switch (a)
		{
		case 1:
			Sort();
			system("cls");
			break;
		case 2:
			Add();
			system("cls");
			break;
		case 3:
			Delete();
			system("cls");
			break;
		case 4:
			Modify();
			system("cls");
			break;
		case 5:
			Search();
			system("cls");
			break;
		case 6:
			printf("\n已退出程序啦,欢迎下次使用，古得拜！！！");
			system("pause");
			return;
		default:
			printf("错误操作请重新输入：");
			getchar();
			getchar();
			system("cls");
			break;
		}
	}
}

void Sort() //排序
{
	int c = 0;
	printf("\n您需要下列哪种排序方式呢？\n\n");
	printf("(1) 编号排序    （2）姓名排序\n");
	scanf("%d", &c);
	CAI t;
	switch (c)
	{
	case 1://编号排序
		for (int i = 0; i < s - 1; i++)
		{
			for (int j = 0; j < s - 1 - i; j++)
			{
				if (book[j].number > book[j + 1].number)
				{
					t = book[j + 1];
					book[j + 1] = book[j];
					book[j] = t;
				}
			}
		}
		break;
	case 2://姓名排序
		for (int i = 0; i < s - 1; i++)
		{
			for (int j = 0; j < s - 1 - j; j++)
			{
				if (strcmp(book[j].name,book[j + 1].name) > 0)
				{
					t = book[j + 1];
					book[j + 1] = book[j];
					book[j] = t;
				}
			}
		}
		break;
	default:
		printf("错误操作请重新输入：");
		getchar();
		getchar();
		system("pause");
		break;
	}
}

void Add()//创建
{
	int i = 0, ret = 0;
	if (b == 0)
	{
		printf("通讯录已满\n");
		getchar();
		getchar();
	}
	else
	{ 
			printf("\n添加操作：\n");
			printf("\n请输入添加位置：");
			scanf("%d", &book[s].number);
			if (book[s].number > 0 && book[s].number <= 50)
			{
				for (; i < s; i++)
				{
					if (book[i].number == book[s].number)
					{
						printf("这里已经有数据了哟亲！\n");
						getchar();
						getchar();
						ret = 1;
					}
				}
				if (ret == 0)
				{
					printf("请输入联系人姓名：");
					scanf("%s",book[s].name);
					printf("\n请输入联系人电话号码：");
					scanf("%s",book[s].phone);
					s++;
					b--;
				}
			}
			else
			{
				printf("\n处理编号超过阈值\n");
				system("\npause");
			}
	} 
}

void Delete()//删除
{
	int k, i = 0, j = 0, ret = 0;
	printf("\n删除操作：\n");
	printf("\n请您输入需要删除的联系人的编号： ");
	scanf("%d", &k);
	if (k > 0 && k <= 50)
	{
		for (; i < s; i++)
		{
			if (book[i].number == k)
			{
				for (j = i; j < s; j++)
				{
					book[j] = book[j+1];
				}
				s--;
				b++;
				ret = 1;
			}
		}
		if (ret == 0)
		{
			printf("这里没有数据哟亲！\n");
			getchar();
			getchar();
			return;
		}
	}
	else
	{
		printf("处理编号超过阈值\n");
		getchar();
		getchar();
		return;
	}
}

void Modify()//修改
{
	int k, i = 0, ret = 0;
	printf("请输入修改位置的编号： ");
	scanf("%d", &k);
	if (k > 0 && k <= 50)
	{
		for (; i < s; i++)
		{
			if (book[i].number == k)
			{
				printf("已移除原有联系人的信息，请您重新输入：\n");
				printf("\n首先请您输入新联系人的姓名： ");
				scanf("%s", book[i].name);
				printf("\n再请输入新联系人的电话号码： ");
				scanf("%s", book[i].phone);
				ret = 1;
			}
		}
		if (ret == 0)
		{
			printf("这里没有数据哟亲！\n");
			getchar();
			getchar();
			return;
		}
	}
	else
	{
		printf("处理编号超过阈值\n");
		getchar();
		getchar();
		return;
	}
}

void Search()//查找
{
	int i = 0, ret = 0;
	char str[12];

	printf("请您输入要查找的联系人的姓名或电话号码： ");
	scanf("%s", str);
		for (; i < s; i++)
		{
			if (strcmp(book[i].name, str) == 0 || strcmp(book[i].phone, str) == 0)
			{
				printf("编号:  %2d  |  姓名:  %8s  |  电话:  %12s\n", book[i].number, book[i].name, book[i].phone);
				getchar();
				getchar();
				ret = 1;
				system("pause");
				return;
			}
		}
		if (ret == 0)
		{
			printf("查无此人\n");
			system("pause");
			return;
		}
}
