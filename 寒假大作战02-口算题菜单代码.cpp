#include<stdio.h>
void menu();
void help();
void error();
void First();
void Second();
void Third();
int main()
{
	int n;
	printf("==========口算生成器==========\n");
	printf("欢迎使用口算生成器:");
	printf("\n");
	printf("\n");
	help();
	printf("\n");
	menu();
	scanf_s("%d", &n);
	while (n != 5)
	{
		printf("<执行操作:\n");
		printf("\n");
		switch (n)
		{
		 case 1:
			 First();
			 break;
		 case 2:
			 Second();
			 break;
		 case 3:
			 Third();
			 break;
		 case 4:
			 help();
			 break;
		 default:
			 error();
			 break;
		}
		printf("\n");
		printf("\n");
		menu();
		scanf_s("%d", &n);
	}
	printf("<执行操作:\n");
	printf("\n");
	printf("程序结束，欢迎下次使用，按任意键结束\n");
	return 0;
}
void help()
{
	printf("帮助信息：\n");
	printf("您需要输入命令代号来进行操作，且\n");
	printf("一年级题目为不超过十位的加减法；\n");
	printf("二年级题目为不超过百位的乘除法；\n");
	printf("三年级题目为不超过百位的加减乘除混合题目.\n");
}
void menu()
{
	printf("操作列表：\n");
	printf("1)一年级    2）二年级   3）三年级\n");
	printf("4)帮助      5）退出程序\n");
	printf("请输入操作>\n");
	printf("<执行操作 ：");
}
void error()
{
	printf("Error!!!\n");
	printf("错误操作指令，请重新输入\n");
}
void First()
{
	printf("现在是一年级题目：");
	printf("执行完了（小声，假装这里有操作）");
}
void Second()
{
	printf("现在是二年级题目：");
	printf("执行完了（小声，假装这里有操作）");
}
void Third()
{
	printf("现在是三年级题目：");
	printf("执行完了（小声，假装这里有操作）");
}